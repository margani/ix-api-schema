"""
Service Constants
-----------------
"""

#
# Network Features
#
NETWORK_FEATURE_TYPE_BLACKHOLING = "blackholing"
NETWORK_FEATURE_TYPE_ROUTESERVER = "route_server"
NETWORK_FEATURE_TYPE_IXPROUTER = "ixp_router"

NETWORK_FEATURE_TYPES = [
    NETWORK_FEATURE_TYPE_BLACKHOLING,
    NETWORK_FEATURE_TYPE_ROUTESERVER,
    NETWORK_FEATURE_TYPE_IXPROUTER,
]


#
# Network Services
#
NETWORK_SERVICE_TYPE_EXCHANGE_LAN = "exchange_lan"
NETWORK_SERVICE_TYPE_E_LINE = "e_line"
NETWORK_SERVICE_TYPE_E_TREE = "e_tree"
NETWORK_SERVICE_TYPE_E_LAN = "e_lan"

NETWORK_SERVICE_TYPES = [
    NETWORK_SERVICE_TYPE_EXCHANGE_LAN,
    NETWORK_SERVICE_TYPE_E_LINE,
    NETWORK_SERVICE_TYPE_E_TREE,
    NETWORK_SERVICE_TYPE_E_LAN,
]

#
# Member joining rules
#
MEMBER_JOINING_RULE_TYPE_WHITELIST = "whitelist"
MEMBER_JOINING_RULE_TYPE_BLACKLIST = "blacklist"

MEMBER_JOINING_RULE_TYPES = [
    MEMBER_JOINING_RULE_TYPE_WHITELIST,
    MEMBER_JOINING_RULE_TYPE_BLACKLIST,
]

