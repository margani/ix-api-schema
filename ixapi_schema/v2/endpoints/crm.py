
"""
CRM
---

Manage accounts, roles and contacts.
"""

from ixapi_schema.openapi import components
from ixapi_schema.v2.entities import crm, problems
from ixapi_schema.v2 import filters


PATHS = {
    #
    # Accounts
    #
    "/accounts": {
        "description": """
            An `Account` is a company using services from an IXP.
            The account can have a hierarchy. An account can have
            subaccounts ("subcustomers"). Each account needs to have
            different contacts, depending on the exchange and
            the service he wants to use.

            For an account to become operational, you need to associate
            a contact with a `legal` role.
        """,
        "GET": {
            "description": """
                Retrieve a list of accounts.

                This includes all accounts the current authorized account
                is managing and the current account itself.
            """,
            "responses": {
                200: crm.Account(many=True),
            },
            "problems": problems.PROTECTED_COLLECTION,
            "filters": components.FilterSet(
                filters.COLLECTION,
                filters.STATEFUL, {
                    "managing_account": components.CharFilter(),
                    "billable": components.NumberFilter(),
                    "external_ref": components.CharFilter(),
                    "name": components.CharFilter(),
                }),
        },
        "POST": {
            "description": """
                Create a new account.

                Please remember that an account may require some `contacts`
                to become fully active. Until then it will stay in an `error` state.
            """,
            "request": crm.AccountRequest(),
            "responses": {
                201: crm.Account(),
            },
            "problems": problems.PROTECTED_COLLECTION_CREATE,
        },
    },
    "/accounts/<id:str>": {
        "description": """
            An `Account` is a company using services from an IXP.
            The accounts can have a hierarchy. An account can have
            subaccounts ("subcustomers"). Each account needs to have
            different contacts, depending on the exchange and
            the service he wants to use.

            For an account to become operational, you need to associate
            a contact with a `legal` role.
        """,
        "GET": {
            "description": "Get a single account.",
            "responses": {
                200: crm.Account(),
            },
            "problems": problems.PROTECTED_RESOURCE,
        },
        "PUT": {
            "description": "Update the entire account.",
            "request": crm.AccountUpdate(),
            "responses": {
                200: crm.Account(),
            },
            "problems": problems.PROTECTED_RESOURCE_UPDATE,
        },
        "PATCH": {
            "description": "Update the entire account.",
            "request": crm.AccountUpdate(partial=True),
            "responses": {
                200: crm.Account(),
            },
            "problems": problems.PROTECTED_RESOURCE_UPDATE,
        }
    },
    #
    # Roles
    #
    "/roles": {
        "description": """
            A `Role` enables a `Contact` to act as a provider
            for a specific set of information.
        """,
        "GET": {
            "description": """
                List all roles available.
            """,
            "responses": {
                200: crm.Role(many=True),
            },
            "problems":  [
                problems.AuthenticationProblem(),
                problems.SignatureExpiredProblem(),
            ],
            "filters": components.FilterSet(
                filters.COLLECTION,
                {"name": components.CharFilter()},
                {"contact": components.CharFilter()},
            ),
        }
    },
    "/roles/<id:str>": {
        "description": """
            A single `Role` enables a `Contact` to act as a provider
            for a specific set of information.

            These are usually `implementation`, `noc`, `legal` or `peering`.
        """,
        "GET": {
            "description": """
                Get a single `Role`.
            """,
            "responses": {
                200: crm.Role(),
            },
            "problems":  [
                problems.AuthenticationProblem(),
                problems.SignatureExpiredProblem(),
            ],
            "filters": components.FilterSet(
                filters.COLLECTION,
                {"name": components.CharFilter()},
            ),
        }
    },
    #
    # Contacts
    #
    "/contacts": {
        "description": """
            A `Contact` is a role undertaking a specific responsibility
            within a customer, typically a department or agent of the
            customer company.

            These can be `implementation`, `noc`, `legal`, `peering`
            or `billing`.

            A contact is bound to the account by the `consuming_account`
            property.
        """,
        "GET": {
            "description": """
                List available contacts managed by the authorized account.
            """,
            "responses": {
                200: crm.Contact(many=True),
            },
            "problems": problems.PROTECTED_COLLECTION,
            "filters": components.FilterSet(
                filters.COLLECTION,
                filters.OWNABLE,
                {"external_ref": components.CharFilter()},
            ),
        },
        "POST": {
            "description": """
                Create a new contact.

                Please remember to set the correct `type` of the contact
                when sending the request. Available types are `noc`, `billing`,
                `legal` and `implementation`.
            """,
            "request": crm.ContactRequest(),
            "responses": {
                201: crm.Contact(),
            },
            "problems": problems.PROTECTED_COLLECTION_CREATE,
        },
    },
    "/contacts/<id:str>": {
        "description": """
            A `Contact` is a role undertaking a specific responsibility
            within an account, typically a department or agent of the
            company.

            A contact is bound to the account by the `consuming_account`
            property and can have many assigned `Role`s.
        """,
        "GET": {
            "description": """Get a contact by it's id""",
            "responses": {
                200: crm.Contact(),
            },
            "problems": problems.PROTECTED_RESOURCE,
        },
        "PUT": {
            "description": """Update a contact""",
            "request": crm.ContactRequest(),
            "responses": {
                200: crm.Contact(),
            },
            "problems": problems.PROTECTED_RESOURCE_UPDATE,
        },
        "PATCH": {
            "description": """Update parts of a contact""",
            "request": crm.ContactRequest(partial=True),
            "responses": {
                200: crm.Contact(),
            },
            "problems": problems.PROTECTED_RESOURCE_UPDATE,
        },
        "DELETE": {
            "description": """Remove a contact""",
            "responses": {
                200: crm.Contact(),
            },
            "problems": problems.PROTECTED_RESOURCE,
        },
    },
    #
    # Role Assignments
    #
    "/role-assignments": {
        "description": """
            A `Contact` can be assigned to many `Roles`.
        """,
        "GET": {
            "description": """
                List all role assignments for a contact.
            """,
            "responses": {
                200: crm.RoleAssignment(many=True),
            },
            "problems": problems.PROTECTED_COLLECTION,
            "filters": components.FilterSet(
                filters.COLLECTION,
                {"contact": components.CharFilter()},
            ),
        },
        "POST": {
            "description": """
                Assign a role to a contact.

                The contact needs to have all fields filled, which the
                role requires. If this is not the case an `UnableToFulfill` error
                will be returned.
            """,
            "request": crm.RoleAssignmentRequest(),
            "responses": {
                201: crm.RoleAssignment(),
            },
            "problems": problems.PROTECTED_COLLECTION_CREATE + [
                problems.UnableToFulfillProblem(),
            ],
        }
    },
    "/role-assignments/<assignment_id:str>": {
        "endpoint": "role-assignments",
        "description": """
            A `Contact` can be assigned to many `Roles`.
        """,
        "GET": {
            "description": """
                Get a role assignment for a contact.
            """,
            "responses": {
                200: crm.RoleAssignment(),
            },
            "problems": problems.PROTECTED_RESOURCE,
        },
        "DELETE": {
            "description": """
                Remove a role assignment a role to a contact.

                If the contact is still in use with a given role required,
                this will yield an `UnableToFulfill` error.
            """,
            "responses": {
                200: crm.RoleAssignment(),
            },
            "problems": problems.PROTECTED_RESOURCE + [
                problems.UnableToFulfillProblem(),
            ],
        }
    },
}

