
"""
This file describes the api spec base information.
This will be included in the openapi schema.
"""

from ixapi_schema import v2


SPEC = {
    "info": {
        "version": v2.__version__,
        "title": "IX-API",
        "description": (
            "This API allows to config/change/delete "
            "Internet Exchange services"),
        "contact": {
            "url": "https://ix-api.net",
            "email": "team@ix-api.net",
        },
    },
    "servers": [{
        "url": "/api/v2",
    }],
    "error_documents_base_url":
        "https://errors.ix-api.net/v2/",
}

