
from ixapi_schema.openapi.components import utils


def test_reverse_mapping():
    """Test reverse mapping"""
    mapping = {
        "foo": 23,
        "bar": "baz",
    }

    inverse = utils.reverse_mapping(mapping)

    assert inverse[23] == "foo"
    assert inverse["baz"] == "bar"


