
"""
Filter stubs
"""

class Filter:
    def __init__(self, *args, **kwargs):
        self.args = args
        self.label = None
        self.extra = kwargs
        for k, v in kwargs.items():
            setattr(self, k, v)


# Filter field stubs
class BulkIdFilter(Filter):
    pass

class CharFilter(Filter):
    pass

class NumberFilter(Filter):
    pass

class ChoiceFilter(Filter):
    def __init__(self, choices, *args, **kwargs):
        """Initialize choice filter"""
        super().__init__(*args, **kwargs)
        self.extra["choices"] = choices
        self.choices = choices

class DateTimeFilter(Filter):
    pass

class ModelChoiceFilter(ChoiceFilter):
    pass

class ModelMultipleChoiceFilter(ChoiceFilter):
    pass


def FilterSet(*filters):
    """
    Merge all filter dics in filters.
    """
    # This function is named in capital camel case 
    # for entirely aesthetic reasons.
    result = {}
    for f in filters:
        result.update(f)
    return result

