
"""
This file describes the api spec base information.
This will be included in the openapi schema.
"""

from ixapi_schema import v1


SPEC = {
    "info": {
        "version": v1.__version__,
        "title": "IX-API",
        "description": (
            "This API allows to config/change/delete "
            "Internet Exchange services"),
        "contact": {
            "url": "https://ix-api.net",
            "email": "team@ix-api.net",
        },
    },
    "servers": [{
        "url": "/api/v1",
    }],
    "error_documents_base_url":
        "https://errors.ix-api.net/v1/",
}

