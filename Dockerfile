

#
# Container for the ix-api schema app
#
FROM python:3.6
ENV PYTHONUNBUFFERED 1
ENV IX_API_SCHEMA_LISTEN_DEFAULT "0.0.0.0:8080"

RUN mkdir -p /ix-api-schema
COPY ./requirements.txt /ix-api-schema/requirements.txt

WORKDIR /ix-api-schema
RUN pip3 install -r requirements.txt

# Copy application
WORKDIR /ix-api-schema
COPY . .

EXPOSE 8080

ENTRYPOINT ["python3", "manage.py"]

